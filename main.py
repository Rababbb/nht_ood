'''This is the repo which contains the original code to the WACV 2021 paper
"Same Same But DifferNet: Semi-Supervised Defect Detection with Normalizing Flows"
by Marco Rudolph, Bastian Wandt and Bodo Rosenhahn.
For further information contact Marco Rudolph (rudolph@tnt.uni-hannover.de)'''

import config as c
from train import train
from utils import  make_dataloaders, load_dataset #load_datasets,
import valohai as vh


#dataset_path ="s3://advian-nht/dataset/T445591/middle"

data_inputs={
			'train':c.dataset_path+'/train/images/*',
			'validation': c.dataset_path+'/validation_defects_only/*'
			}


vh.prepare(step='train-differ', image='pytorch/pytorch:1.3-cuda10.1-cudnn7-runtime',default_inputs=data_inputs)

#train_set, test_set = load_datasets(data_inputs,c.class_name)
train_set = load_dataset(vh.inputs('train').path())
test_set = load_dataset(vh.inputs('validation').path()),test=True)
train_loader, test_loader = make_dataloaders(train_set, test_set)
model = train(train_loader, test_loader)


save_path = valohai.outputs().path('differ_first.h5')
model.save(save_path)
